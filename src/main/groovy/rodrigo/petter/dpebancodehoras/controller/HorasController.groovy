package rodrigo.petter.dpebancodehoras.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import rodrigo.petter.dpebancodehoras.model.entity.Dia
import rodrigo.petter.dpebancodehoras.model.entity.Mes
import rodrigo.petter.dpebancodehoras.model.entity.Periodo

import java.time.LocalTime
import java.time.Month

@Controller
@RequestMapping(path = "horas")
class HorasController {

    @GetMapping(path = '/new')
    String novo(Model model) {

        Mes mes = new Mes(ano: 2020, mes: Month.MARCH, dias: [
                new Dia(dia: 1, periodos:
                        [
                                new Periodo(LocalTime.of(22,00), LocalTime.of(23,00)),
                                new Periodo(LocalTime.of(21,00), LocalTime.of(21,56))
                        ]
                )
        ])

        model.addAttribute('mes', mes)

        return 'horas/new_template'
    }

    @RequestMapping(value="/new", params = ["addRow"])
    String addRow(final Mes mes, final BindingResult bindingResult) {
        //mes.dias.
        return 'horas/new_template'
    }


    @ModelAttribute("MesesEnum")
    List<Month> meses() {
        return Month.findAll()

    }
}

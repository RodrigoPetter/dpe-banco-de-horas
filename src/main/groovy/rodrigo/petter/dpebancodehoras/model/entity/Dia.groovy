package rodrigo.petter.dpebancodehoras.model.entity

import groovy.transform.Canonical

@Canonical
class Dia {

    Integer dia
    Periodo[] periodos
}

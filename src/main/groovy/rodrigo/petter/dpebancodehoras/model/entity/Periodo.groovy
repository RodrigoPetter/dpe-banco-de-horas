package rodrigo.petter.dpebancodehoras.model.entity

import groovy.transform.Canonical

import java.time.LocalTime

@Canonical
class Periodo {

    LocalTime inicio
    LocalTime fim

}
